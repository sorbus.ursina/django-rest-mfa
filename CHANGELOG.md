# 1.2.0

- Add Django admin integration for trusted device (remember me)
- Add configurable cookie name and max days for trusted device

# 1.1.0

- Update to fido2 1.0.0 package

# 1.0.0

- First stable release
- Add support for making GET request to user key backup codes to generate backup codes without saving to the database. This can be useful when showing the codes to the user before committing. For example, it may be desirable to have the user enter one code to prove they copied them. But this needs to happen before actually committing to MFA.
- POST to backup code user key now allows users to set the codes. This could be useful for client side code generation.
